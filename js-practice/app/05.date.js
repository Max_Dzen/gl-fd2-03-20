console.group("Topic: Date object");

// Task 1
// RU: Создать текущую дату и вывести ее в формате dd.mm.yyyy и dd Month yyyy
// EN: Create current date and display it in the console according to the format
//     dd.mm.yyyy и dd Month yyyy
{
  console.group("task 1");

  const justNow1 = new Date(2020, 01, 27);
  const justNow2 = new Date(2020, 02, 27);
  console.log(justNow1.toLocaleDateString());
  console.log(
    justNow2.getUTCDate(),
    justNow2.getUTCMonth(),
    justNow2.getUTCFullYear()
  );

  console.groupEnd();
}

// Task 2
// RU: Создать объект Date из строки '15.03.2025'.
// EN: Create an object Date from the string '15.03.2025'.
{
  console.group("task 2");

  const str = "15.03.2025";

  function createDate(date) {
    const arr = date.split(".");
    const justNow = new Date(
      arr[arr.length - 1],
      arr[arr.length - 2],
      arr[arr.length - 3]
    );
    return justNow;
  }
  console.log(createDate(str));

  console.groupEnd();
}

// Task 3
// RU: Создать объект Date, который содержит:
//     1. завтрашнюю дату,
//     2. первое число текущего месяца,
//     3. последнее число текущего месяца
// EN: Create an object Date, which represents:
//     1. tomorrow
//     2. first day of the current month
//     3. last day of the current month
{
  console.group("task 3");
  function tomorrowDay() {
    const tomorrow = new Date();
    tomorrow.setUTCDate(tomorrow.getDate() + 1);
    return `Завтрашняя дата: ${tomorrow}`;
  }
  console.log(tomorrowDay());

  function firstDay() {
    const dateNow = new Date();
    const dayFirst = dateNow.getDate() - (dateNow.getDate() - 1);
    return `Первое число текущего месяца: ${dayFirst}`;
  }
  console.log(firstDay());

  function lastDay() {
    const dateNow = new Date();
    const currentMonth = dateNow.getMonth();
    const currentYear = dateNow.getFullYear();
    switch (currentMonth) {
      case 0:
      case 2:
      case 4:
      case 6:
      case 7:
      case 9:
      case 11: {
        return "Последнее число текущего месяца: 31";
      }
      case 1: {
        if (currentYear % 4 === 0) {
          return "Последнее число текущего месяца: 29";
        } else {
          return "Последнее число текущего месяца: 28";
        }
      }
      case 3:
      case 5:
      case 8:
      case 10: {
        return "Последнее число текущего месяца: 30";
      }
    }
  }
  console.log(lastDay());

  console.groupEnd();
}

// Task 4
// RU: Подсчитать время суммирования чисел от 1 до 1000.
// EN: Calculate the time of summing numbers from 1 to 1000.
{
  console.group("task 4");

  function calculationSum() {
    const dateFirst = new Date();
    let sum = 0;

    for (let i = 0; i <= 1000; i++) {
      sum += i;
    }

    const dateLast = new Date();
    const countingTime = dateLast - dateFirst;

    console.log(countingTime);
  }
  calculationSum();

  console.groupEnd();
}

// Task 5
// RU: Подсчитать количество дней с текущей даты до Нового года.
// EN: Calculate the number of days from the current date to the New Year.
{
  console.group("task 5");

  function calculationDays() {
    const dateNow = new Date();
    const yearNow = dateNow.getFullYear();
    const dateFirst = new Date(yearNow, 11, 31);
    const timeDifference = dateFirst - dateNow;
    const amountOfDays = timeDifference / 3600000 / 24;

    return `Количество дней с текущей даты до Нового года: ${Math.round(amountOfDays)}`;
  }

  console.log(calculationDays());

  console.groupEnd();
}

console.groupEnd();
