console.group("Topic: Primitive Data Types");

// Task 01
// Объявите переменную days и проинициализируйте ее числом от 1 до 10.
// Преобразуйте это число в количество секунд и выведите в консоль.

{
  console.group("task 01");

  const days = 5;

  console.log(`${days} дней равняется ${days * 24 * 60 * 60} секунд`);

  console.groupEnd();
}
// Task 02
// Объявите две переменные: admin и name. Установите значение переменной name
// в ваше имя. Скопируйте это значение в переменную admin и выведите его в консоль.

{
  console.group("task 02");

  const name = "Maxim";
  const admin = name;

  console.log(admin);

  console.groupEnd();
}
// Task 03
// Объявите три переменных: a, b, c. Присвойте им следующие значения: 10, 2, 5.
// Объявите переменную result1 и вычислите сумму значений переменных a, b, c.
// Объявите переменную min и вычислите минимальное значение переменных a, b, c.
// Выведите результат в консоль.

{
  console.group("task 03");

  const a = 10;
  const b = 2;
  const c = 5;
  const result1 = a + b + c;
  const min = Math.min(a, b, c);

  console.log(
    `Сумма ${a}, ${b}, ${c} равна: ${result1}, минимальное значение из ${a}, ${b}, ${c}, равняется: ${min}`
  );

  console.groupEnd();
}
// Task 04
// Объявите три переменных: hour, minute, second. Присвойте им следующие значения:
// 10, 40, 25. Выведите в консоль время в формате 10:40:25.

{
  console.group("task 04");

  const hour = 10;
  const minute = 40;
  const second = 25;

  console.log(`${hour}:${minute}:${second}`);

  console.groupEnd();
}
// Task 05
// Объявите переменную minute и проинициализируйте ее целым числом.
// Вычислите к какой четверти принадлежит это число и выведите в консоль.

{
  console.group("task 05");

  const minute = 10;
  let quarter;

  minute > 0 && minute <= 15
    ? (quarter = 1)
    : minute > 15 && minute <= 30
    ? (quarter = 2)
    : minute > 30 && minute <= 45
    ? (quarter = 3)
    : (quarter = 4);

  console.log(`${quarter}-я четверть`);

  console.groupEnd();
}
// Task 06
// Объявите две переменные, которые содержат стоимость товаров:
// первый товар - 0.10 USD, второй - 0.20 USD
// Вычислите сумму и выведите в консоль. Используйте toFixed()

{
  console.group("task 06");

  const cheese = "0.10 USD";
  const bread = "0.20 USD";
  const result = (parseFloat(cheese) + parseFloat(bread)).toFixed(2);

  console.log(`Сумма ${cheese} и ${bread} равна ${result} USD`);

  console.groupEnd();
}
// Task 07
// Объявите переменную a.
// Если значение переменной равно 0, выведите в консоль "True", иначе "False".
// Проверьте, что будет появляться в консоли для значений 1, 0, -3.

{
  console.group("task 07");

  const a = 1;

  const result = a === 0 ? true : false;

  console.log(result);

  console.groupEnd();
}
// Task 08
// Объявите две переменных: a, b. Вычислите их сумму и присвойте переменной result.
// Если результат больше 5, выведите его в консоль, иначе умножте его на 10
// и выведите в консоль.
// Данные для тестирования: 2, 5 и 3, 1.

{
  console.group("task 08");

  const a = 2;
  const b = 5;
  const sum = a + b;

  const result = sum > 5 ? sum : sum * 10;

  console.log(result);

  console.groupEnd();
}
// Task 09
// Объявите переменную month и проинициализируйте ее числом от 1 до 12.
// Вычислите время года и выведите его в консоль.

{
  console.group("task 09");

  const month = 12;

  month > 2 && month <= 5
    ? console.log("Весна")
    : month > 5 && month <= 8
    ? console.log("Лето")
    : month > 8 && month <= 11
    ? console.log("Осень")
    : console.log("Зима");

  console.groupEnd();
}
// Task 10
// Выведите в консоль все числа от 1 до 10.

{
  console.group("task 10");

  for (let a = 1; a <= 10; a++) {
    console.log(a);
  }

  console.groupEnd();
}
// Task 11
// Выведите в консоль все четные числа от 1 до 15.

{
  console.group("task 11");

  for (let even = 1; even <= 15; even++) {
    if (even % 2 === 0) {
      console.log(even);
    }
  }

  console.groupEnd();
}
// Task 12
// Нарисуйте в консоле пирамиду на 10 уровней как показано ниже
// x
// xx
// xxx
// xxxx
// ...

{
  console.group("task 12");

  for (let i = 1; i <= 10; i++) {
    console.log("x".repeat(i));
  }

  console.groupEnd();
}
// Task 13
// Нарисуйте в консоле пирамиду на 9 уровней как показано ниже
// 1
// 22
// 333
// 4444
// ...

{
  console.group("task 13");

  for (let i = 1; i <= 9; i++) {
    console.log(i.toFixed().repeat(i));
  }

  console.groupEnd();
}
// Task 14
// Запросите у пользователя какое либо значение и выведите его в консоль.

{
  console.group("task 14");

  const name = prompt("Как тебя зовут?");

  console.log(name);

  console.groupEnd();
}
// Task 15
// Перепишите if используя тернарный опертор
// if (a + b < 4) {
//   result = 'Мало';
// } else {
//   result = 'Много';
// }

{
  console.group("task 15");

  const result = a + b < 4 ? "Мало" : "Много";

  console.groupEnd();
}
// Task 16
// Перепишите if..else используя несколько тернарных операторов.
// var message;
// if (login == 'Вася') {
//   message = 'Привет';
// } else if (login == 'Директор') {
//   message = 'Здравствуйте';
// } else if (login == '') {
//   message = 'Нет логина';
// } else {
//   message = '';
// }

{
  console.group("task 16");

  const message =
    login === "Вася"
      ? "Привет"
      : login === "Директор"
      ? "Здравствуйте"
      : login === ""
      ? "Нет логина"
      : "";

  console.groupEnd();
}
// Task 17
// Замените for на while без изменения поведения цикла
// for (var i = 0; i < 3; i++) {
//   alert( "номер " + i + "!" );
// }

{
  console.group("task 17");

  let i = 0;

  while (i < 4) {
    alert("номер " + i + "!");
    i++;
  }

  console.groupEnd();
}
// Task 18
// Напишите цикл, который предлагает prompt ввести число, большее 100.
// Если пользователь ввёл другое число – попросить ввести ещё раз, и так далее.
// Цикл должен спрашивать число пока либо посетитель не введёт число,
// большее 100, либо не нажмёт кнопку Cancel (ESC).
// Предусматривать обработку нечисловых строк в этой задаче необязательно.

{
  console.group("task 18");

  for (let i = 1; ; i++) {
    const bigNumber = prompt("Введите число, большее 100!");
    if (bigNumber > 100 || bigNumber === null) break;
  }

  console.groupEnd();
}
// Task 19
// Переписать следующий код используя switch
// var a = +prompt('a?', '');
// if (a == 0) {
//   alert( 0 );
// }
// if (a == 1) {
//   alert( 1 );
// }
// if (a == 2 || a == 3) {
//   alert( '2,3' );
// }

{
  console.group("task 19");

  let a = +prompt("a?", "");

  switch (a) {
    case 0: {
      alert(0);
      break;
    }
    case 1: {
      alert(1);
      break;
    }
    case 2: {
      alert("2, 3");
      break;
    }
    case 3: {
      alert("2, 3");
      break;
    }
  }

  console.groupEnd();
}
// Task 20
// Объявите переменную и проинициализируйте ее строчным значением в переменном
// регистре. (Например так "таООооОддОО")
// Напишите код, который преобразует эту строку к виду:
// первая буква в верхнем регистре, остальные буквы в нижнем регистре.
// Выведите результат работы в консоль
// Используйте: toUpperCase/toLowerCase, slice.

{
  console.group("task 20");

  const str = "таООооОддОО";
  console.log(str.slice(0, 1).toUpperCase() + str.slice(1).toLowerCase());

  console.groupEnd();
}
// Task 21
// Напишите код, который выводит в консоль true, если строка str содержит
// „viagra“ или „XXX“, а иначе false.
// Тестовые данные: 'buy ViAgRA now', 'free xxxxx'

{
  console.group("task 21");

  const str1 = "buy ViAgRA now".toLowerCase();
  const str2 = "free xxxxx".toUpperCase();

  function search(a) {
    const result =
      a.includes("viagra") === true || a.includes("XXX") === true
        ? true
        : false;

    console.log(result);
  }

  search(str1);
  search(str2);

  console.groupEnd();
}
// Task 22
// Напишите код, который проверяет длину строки str, и если она превосходит
// maxlength – заменяет конец str на "...", так чтобы ее длина стала равна maxlength.
// Результатом должна быть (при необходимости) усечённая строка.
// Выведите строку в консоль
// Тестовые данные:
//  "Вот, что мне хотелось бы сказать на эту тему:", 20
//  "Всем привет!", 20

{
  console.group("task 22");

  const str1 = "Вот, что мне хотелось бы сказать на эту тему:";
  const str2 = "Всем привет!";
  const maxlength = 20;

  function lineTruncation(a) {
    const result = a.length > maxlength ? a.slice(0, maxlength - 3) + "..." : a;
    return result;
  }

  console.log(lineTruncation(str1));
  console.log(lineTruncation(str2));

  console.groupEnd();
}
// Task 23
// Напишите код, который из строки $100 получит число и выведите его в консоль.

{
  console.group("task 23");

  const str = "$100";

  function conversion(a) {
    return parseFloat(a.slice(1, 4));
  }

  console.log(conversion(str));

  console.groupEnd();
}
// Task 24
// Напишите код, который проверит, является ли переменная промисом

{
  console.group("task 24");

  const promise = new Promise((res, rej) => {});

  console.log(typeof promise);

  console.groupEnd();
}
console.groupEnd();
