console.group("Topic: Functions");

// Task 1. FDS
// RU: Создайте функцию conc, которая должна конкатенировать значения
//     двух параметров a и b и возвращать строку.
//     Используйте Function Declaration Statement (FDS).
//     Вызовите функцию до ее объявления.
//     Тестовые данные:
//     a = '1', b = '1', result = '11'
//     a = 1, b = 1, result = '11'
// EN: Create a function conc, which should concatenate the values
//     of two parameters a and b and return a string.
//     Use Function Declaration Statement (FDS).
//     Call a function before it declaration.
//     Test data:
//     a = '1', b = '1', result = '11'
//     a = 1, b = 1, result = '11'

{
  console.group("task 1");

  console.log(conc(1, 1));
  console.log(conc("1", "1"));

  function conc(a, b) {
    return `${a}${b}`;
  }

  console.groupEnd();
}
// Task 2. FDE
// RU: Создайте функцию comp, которая должна сравнивать значения
//     двух параметров a и b и возвращать 1, если они равны и -1, если они не равны.
//     Используйте Function Definition Expression (FDE).
//     Вызовите функцию после ее объявления.
//     Тестовые данные:
//     a = 'abc', b = 'abc', result = 1
//     a = 'abC', b = 'abc', result = -1
// EN: Create a function comp, which should compare the values
//     of two parameters a and b, and return 1, when they equal and return -1,
//     when they are not equal.
//     UseFunction Definition Expression (FDE).
//     Call a function after it declaration.
//     Test data:
//     a = 'abc', b = 'abc', result = 1
//     a = 'abC', b = 'abc', result = -1

{
  console.group("task 2");

  const comp = function (a, b) {
    if (a === b) {
      return console.log(1);
    } else {
      return console.log(-1);
    }
  };

  comp("abc", "abc");
  comp("abC", "abc");

  console.groupEnd();
}
// Task 3. AF
// RU: Создайте анонимную функцию, которая должна
//     выводить сообщение 'message in console' в коноль.
//     Используйте ее как обработчик события click для кнопки.
// EN: Create an anonymous function, which should display
//     message 'message in console' in console.
//     Use it as an event handler of event click of the button.
{
  console.group("task 3");

  const btn = document.querySelector("button");
  btn.addEventListener("click", function () {
    console.log("message in console");
  });

  console.groupEnd();
}
// Task 4. NFE
// RU: Создайте функцию fibo, которая должна вычислять числа Фибоначчи по формуле
//     F0 = 0, F1 = 1, Fn = Fn-1 + Fn-2.
//     Создайте функцию factorial, которая должна вычислять факториал числа по формуле
//     Fn = 1 * 2 *..*n.
//     Используйте Named Function Expression (NFE).
// EN: Create a function fibo should calculate Fibonacci numbers according to the formula
//     F0 = 0, F1 = 1, Fn = Fn-1 + Fn-2.
//     Create a function factorial which should calculate factorial of a number n
//     according to the formula Fn = 1 * 2 *..*n.
//     Use Named Function Expression (NFE).
{
  console.group("task 4");

  const fibo = function (a) {
    return a <= 1 ? a : fibo(a - 1) + fibo(a - 2);
  };
  console.log(fibo(3));
  console.log(fibo(7));
  console.log(fibo(10));

  const factorial = function (b) {
    if (b == 0) {
      return 1;
    } else {
      return b * factorial(b - 1);
    }
  };
  console.log(factorial(3));

  console.groupEnd();
}
// Task 5. FC
// RU: Объявите две строчные переменные: params и body и проинициализируйте
//     их строчными значениями, которые представляют список параметров и тело будущей функции.
//     Создайте функцию, используя эту информацию с помощью Function Constructor (FC).
//     Вызовите эту функцию.
// EN: Declare two string variables: params and body and initialize them with string values,
//     which represent the list of parameters and the body of future function.
//     Create function using these variables with help of Function Constructor (FC).
//     Call this function.
{
  console.group("task 5");

  const params = "a, b, c";
  const body = "return console.log(a + b + c);";

  const sum = new Function(params, body);

  sum(1, 2, 3);

  console.groupEnd();
}
// Task 6. ArF
// RU: Объявите массив arr = [1, 8, 3, 5, 12, 7, 9, 11]
//     Используя стрелочные функции создайте новый массив из элементов elem * elem,
//     которые меньше 100 и отсортируйте его по возрастанию.
//     Выведите результат в консоль.
// EN: Declare an array arr = [1, 8, 3, 5, 12, 7, 9, 11]
//     Using arrow functions create new array which contains elem * elem.
//     These elements should be less than 100, sort it in ascending order.
//     Display the result in the console.
{
  console.group("task 6");

  const arr = [1, 8, 3, 5, 12, 7, 9, 11];

  const fn = array => {
    const newArr = array
      .map(elem => elem * elem)
      .filter(elem => elem < 100)
      .sort((a, b) => {
        if (a > b) return 1;
        if (a < b) return -1;
        if (a === b) return 0;
      });
    return newArr;
  };
  console.log(fn(arr));

  console.groupEnd();
}
// Task 7. IIFE
// RU: Создайте конструкцию, с помощью которой выполниться выше реализованная
//     функция conc.
// EN: Create a construction which allows to run the above function conc.
{
  console.group("task 7");

  const arr = [1, 8, 3, 5, 12, 7, 9, 11];

  (function (array) {
    const newArr = array
      .map(elem => elem * elem)
      .filter(elem => elem < 100)
      .sort((a, b) => {
        if (a > b) return 1;
        if (a < b) return -1;
        if (a === b) return 0;
      });
    console.log(newArr);
  })(arr);

  console.groupEnd();
}
// Task 8. Arguments Object, Rest
// RU: Создайте функцию parts, которая принимает неизвестное количество параметров.
//     Каждый параметр – это группа предложений.
//     Функция должна вырезать из параметра подстроку, начиная с символа двоеточие (:)
//     и заканчивая символом точка (.).
//     Функция должна возвращать массив подстрок.
//     Используйте Function Definition Expression (FDE).
//     Тестовые данные:
//     param1 = "This is the first sentence. This is a sentence with a list of items:
//               cherries, oranges, apples, bananas."
//     param2 = "This is the second sentence. This is a sentence with a list of items:
//               red, blue, yellow, black."
//     result = ["cherries, oranges, apples, bananas", "red, blue, yellow, black"].
// EN: Create a function parts, which takes unknown quantity of parameters.
//     Each parameter is a group of sentances.
//     The function should cut out the substring from the parameter, starting with the colon (:)
//     and ending with a period (.) character.
//     The function should return an array of substrings.
//     Use Function Definition Expression (FDE).
//     Test Data:
//     param1 = "This is the first sentence. This is a sentence with a list of items:
//               cherries, oranges, apples, bananas."
//     param2 = "This is the second sentence. This is a sentence with a list of items:
//               red, blue, yellow, black."
//     result = ["cherries, oranges, apples, bananas", "red, blue, yellow, black"].
{
  console.group("task 8");

  const param1 =
    "This is the first sentence. This is a sentence with a list of items: cherries, oranges, apples, bananas.";
  const param2 =
    "This is the second sentence. This is a sentence with a list of items: red, blue, yellow, black.";

  const parts = function (...a) {
    const result = [];

    for (let i = 0; i < a.length; i++) {
      const newStr = a[i].slice(a[i].indexOf(":") + 2, a[i].lastIndexOf("."));
      result.push(newStr);
    }
    return result;
  };

  console.log(parts(param1, param2));

  console.groupEnd();
}
// Task 9. Optional Arguments
// RU: Создайте функцию find(testString, test), которая должна возвращать позицию
//     строки test в строке testString.
//     Если второй параметр не задан, используйте test = testString.
//     Используйте Function Definition Expression (FDE).
//     Тестовые данные:
//     testString = 'abc', test ='b', result = 1
//     testString = 'abc', result = 0
//     testString = 'abc', test = 'd', result = -1
//     testString = 'abc', test='a', test2='b', result = 0
// EN: Create a function find(testString, test), which should return the position
//     of test string within testString.
//     If the second parameter is omitted, use default value  test = testString.
//     Use Function Definition Expression (FDE).
//     Test Data:
//     testString = 'abc', test ='b', result = 1
//     testString = 'abc', result = 0
//     testString = 'abc', test = 'd', result = -1
//     testString = 'abc', test='a', test2='b', result = 0
{
  console.group("task 9");

  const find = function (testString, test) {
    if (!test) {
      test = testString;
      return 0;
    }
    return testString.indexOf(`${test}`);
  };

  console.log(find("abc", "b"));
  console.log(find("abc"));
  console.log(find("abc", "d"));
  console.log(find("abc", "a", "b"));

  console.groupEnd();
}
// Task 10. Function as an Object
// RU: Создайте функцию str(), которая принимает один строчный параметр и
//     выводит в консоль 'String is non empty', если параметр - непустая строка и
//     'String is empty', если параметр – пустая строка.
//     Создайте функцию str.isNonEmptyStr(), как свойство функции str. Эта функция должна
//     принимать один параметр и возвращать true, если параметр непустая строка,
//     иначе false. Используйте эту функцию для реализации условия в основной функции.
//     Тестовые данные:
//     str.isNonEmptyStr(), result = false
//     str.isNonEmptyStr(''), result = false
//     str.isNonEmptyStr('a'), result = true
//     str.isNonEmptyStr(1), result = false
//     str(), console.log('String is empty')
//     str('a'), console.log('String is non empty')
// EN: Create a function str(), which takes one string parameter and display in the console
//     string 'String is non empty' if the paramer is not empty string, otherwise it
//     should display 'String is empty'.
//     Create a function str.isNonEmptyStr() as a property of function str. This function
//     should take one parameter and return true, when the value of parameter is not empty
//     string, otherwise it should return false. Use this function to implement if statement
//     in the str() funtion.
//     Test Data:
//     str.isNonEmptyStr(), result = false
//     str.isNonEmptyStr(''), result = false
//     str.isNonEmptyStr('a'), result = true
//     str.isNonEmptyStr(1), result = false
//     str(), console.log('String is empty')
//     str('a'), console.log('String is non empty')
{
  console.group("task 10");

  const str = function (param) {
    if (Boolean(param)) {
      console.log("String is non empty");
    } else {
      console.log("String is empty");
    }
  };

  str.isNonEmptyStr = function (param) {
    if (typeof param === "number") {
      console.log(false);
    } else if (Boolean(param)) {
      console.log(true);
    } else {
      console.log(false);
    }
  };
  str.isNonEmptyStr();
  str.isNonEmptyStr("");
  str.isNonEmptyStr("a");
  str.isNonEmptyStr(1);
  str();
  str("a");

  console.groupEnd();
}
// Task 11. Function as a Parameter
// RU: Создайте функцию toConsole с одним параметром. Функция должна выводить
//     значение параметра в консоль.
//     Создайте функцию toAlert с одним параметром. Функция должна выводить значение
//     параметра используя alert().
//     Создайте функцию splitToWords с двумя параметрами: msg и callback.
//     Функция должна разделять строку на слова и использовать колбек для отображения слов.
//     Если второй параметр не задан, функция должна возвращать массив слов.
// EN: Create a function toConsole with one parameter. The function should display
//     the value of the parameter in the console.
//     Create a function toAlert with one parameter. The function should display
//     the value of the parameter using alert.
//     Create a function splitToWords, which takes two parameters: msg и callback.
//     The function should split the value of parameter msg by the words and use callback
//     to display these words.
//     If the second parameter is omitted, the function should return array of words.
//     Test Data:
//     splitToWords("My very long text msg", toConsole);
//     result:
//     My
//     very
//     long
//     text
//     msg
//     splitToWords("My very long text msg", toAlert);
//     result = alert(My), ….
//     console.log( splitToWords("My very long text msg") );
//     result = ['My', 'very', 'long', 'text', 'msg']
{
  console.group("task 11");

  const toConsole = param => console.log(param);
  const toAlert = param => alert(param);

  const splitToWords = (msg, callback) => {
    const arr = msg.split(" ");

    if (callback === undefined) return arr;

    for (let i = 0; i < arr.length; i++) {
      callback(arr[i]);
    }
  };
  splitToWords("My very long text msg", toConsole);
  splitToWords("My very long text msg", toAlert);
  console.log(splitToWords("My very long text msg"));

  console.groupEnd();
}
// Task 12. Function as a Result
// RU: Создайте функцию copyright, которая должна возвращать другую функцию с
//     одним параметром. Возращаемая функция должна прибавлять знак © ('\u00A9')
//     вначале своего параметра и возвращать результат. Объявите этот знак в функции copyright.
//     Тестовые данные:
//     console.log( copyright()('EPAM') ); result = © EPAM.
// EN: Create a function copyright, which ahould return anther function with one parameter.
//     This returned function should prepend sign © ('\u00A9') to its parameter and
//     return the result. Declare the sign © ('\u00A9') inside copyright function.
//     Test Data:
//     console.log( copyright()('EPAM') ); result = © EPAM.
{
  console.group("task 12");

  function copyright() {
    const copyright = "\u00A9";
    return function addition(param) {
      return `${copyright} ${param}`;
    };
  }
  console.log(copyright()("EPAM"));

  console.groupEnd();
}
// Task 13. Function as a Result
// RU: Задание аналогично предыдущему, но в этот раз функция copyright получает знак
//     как свой параметр.
// EN: This task is similar to the previous one, but in this case the function copyright takes
//     one parameter - sign (© ('\u00A9')).
{
  console.group("task 13");

  function copyright(cop) {
    return function addition(param) {
      return `${cop} ${param}`;
    };
  }
  console.log(copyright("\u00A9")("EPAM"));

  console.groupEnd();
}
// Task 14. Function as a Method
// RU: Создайте литерал объекта employee со следующими свойствами:
//     name: 'Ann',
//     work – функция, которая выводит в консоль сообщение "I am Ann. I am working..."
//     Тестовые данные
//     employee.work()  результат в консоле "I am Ann. I am working..."
// EN: Create an object literal employee with the following properties:
//     name: 'Ann',
//     work – function, which display in the console the following string
//     "I am Ann. I am working..."
//     Test Data:
//     employee.work()  result in the console "I am Ann. I am working..."
{
  console.group("task 14");

  const employee = {
    name: "Ann",
    work: () => console.log(`I am ${employee.name}. I am working...`)
  };
  employee.work();

  console.groupEnd();
}
// Task 15. Borrow Method
// RU: Создайте литерал объекта person со свойством name.
//     Вызовите метод work объекта employee из предыдущего задания.
// EN: Create an object literal person with property name.
//     Call the method work of the object employee from the previous task.
{
  console.group("task 15");

  const employee = {
    name: "Ann",
    work: function () {
      console.log(`I am ${this.name}. I am working...`);
    }
  };

  employee.work();

  const person = {
    name: "John"
  };

  employee.work.call(person);

  console.groupEnd();
}
// Task 16. Memoization
// RU: Создать функцию fiboMemo для вычисления чисел Фибоначчи по формуле
//     F0 = 0, F1 = 1, Fn = Fn-1 + Fn-2. Функция должна хранить те значения,
//     которые она уже вычисляла. Используя методы console.time(), console.timeEnd()
//     определите время вычисления функции fibo и функции fiboMemo.
// EN: Create a function fiboMemo for calculating Fibonacci numbers according to the formula
//     F0 = 0, F1 = 1, Fn = Fn-1 + Fn-2. The function should store the values computed earlier.
//     Using methods console.time(), console.timeEnd() calculate the time for function fibo
//     and fiboMemo.
{
  console.group("task 16");

  console.time("calculation time fibo");

  const fibo = function (a) {
    return a <= 1 ? a : fibo(a - 1) + fibo(a - 2);
  };

  console.log(fibo(3));
  console.log(fibo(7));
  console.log(fibo(10));

  console.timeEnd("calculation time fibo");

  console.time("calculation time fiboMemo");

  const memoize = fn => {
    const cache = {};
    return (...args) => {
      let n = args[0];
      if (n in cache) {
        return cache[n];
      } else {
        let result = fn(n);
        cache[n] = result;
        return result;
      }
    };
  };

  const fiboMemo = memoize(a => {
    return a <= 1 ? a : fibo(a - 1) + fibo(a - 2);
  });

  console.log(fiboMemo(3));
  console.log(fiboMemo(7));
  console.log(fiboMemo(10));

  console.timeEnd("calculation time fiboMemo");

  console.groupEnd();
}

console.groupEnd();
