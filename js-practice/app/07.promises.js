console.group("Topic: Promises");
// Task 01
// Создайте промис, который постоянно находиться в состоянии pending.
// В конструкторе промиса выведите в консоль сообщение "Promise is created".
{
  console.group("task 01");

  const promise = new Promise((res, rej) => { return console.log("Promise is created") });

  console.groupEnd();
}
// Task 02
// Создайте промис, который после создания сразу же переходит в состояние resolve
// и возвращает строку 'Promise Data'
// Получите данные промиса и выведите их в консоль
{
  console.group("task 02");

  const promise = new Promise((resolve, reject) => resolve("Promise Data"));
  promise.then(message => console.log(message));

  console.groupEnd();
}
// Task 03
// Создайте промис, который после создания сразу же переходит в состояние rejected
// и возвращает строку 'Promise Error'
// Получите данные промиса и выведите их в консоль
{
  console.group("task 03");

  const promise = new Promise((resolve, reject) => reject("Promise Error"));
  promise.catch(message => console.log(message));

  console.groupEnd();
}
// Task 04
// Создайте промис, который переходит в состояние resolved через 3с.
// (Используйте setTimeout) и возвращает строку 'Promise Data'
// Получите данные промиса и выведите их в консоль
{
  console.group("task 04");

  const promise = new Promise((resolve, reject) => setTimeout(() => resolve("Promise Data"), 3000));
  promise.then(message => console.log(message));

  console.groupEnd();
}
// Task 05
// Создайте литерал объекта handlePromise со следующими свойствами:
// promise, resolve, reject, onSuccess, onError
// Проинициализируйте первые три свойства null,
// а последние два функциями, которые принимают один параметр и выводят
// в консоль сообщения: первая - `Promise is resolved with data: ${paramName}`
// вторая - `Promise is rejected with error: ${paramName}`
// Создайте три кнопки и три обработчика события click для этих кнопок
// Первый обработчик, создает промис, заполняет первые три свойства,
// описаного выше объекта: свойство promise получает новый созданный промис,
// свойства resolve и reject получают ссылки на соответствующие функции
// resolve и reject. Следующие два обработчика запускают методы resolve и reject.
{
  console.group("task 05");

  console.groupEnd();
}
// Task 06
// Используйте предыдущее задание. Продублируйте строчку с методом then
{
  console.group("task 06");

  console.groupEnd();
}
// Task 07
// Создайте промис, который через 1 с возвращает строку "My name is".
// Создайте функцию onSuccess, которая получает один параметр,
// прибавляет к нему Ваше имя и возвращает новую строку из функции
// Создайте функцию print, которая выводит в консоль значение своего параметра
// Добавьте два метода then и зарегистрируйте созданные функции.
{
  console.group("task 07");

  const promise = new Promise((resolve, reject) => setTimeout(() => resolve("My name is"), 1000));
  promise.then(onSuccess).then(print);

  function onSuccess(param) {
    return `${param} ${"Max"}`;
  }

  function print(param) {
    return console.log(`${param}`);
  }

  console.groupEnd();
}
// Task 08
// Используйте предыдущий код. Добавьте в функци onSuccess генерацию исключения
// Обработайте даное исключение, используя catch. Обратите внимание,
// что метод print при этом не выполняется.
{
  console.group("task 08");

  console.groupEnd();
}
// Task 09
// Напишите функцию getPromiseData, которая принимает один параметр - промис. Функция получает
// значение промиса и выводит его в консоль
// Объявите объект со свойтвом name и значением Anna.
// Создайте врапер для этого объекта и вызовите для него функцию getPromiseData
{
  console.group("task 09");

  let promise = new Promise(function (resolve, reject) {
    const human = {
      name: "Anna"
    };
    resolve(human);
  });

  function getPromiseData(prom) {
    prom.then(data => {
      console.log(data);
    });
  }

  getPromiseData(promise);

  console.groupEnd();
}
// Task 10
// Создайте два промиса. Первый промис возвращает объект { name: "Anna" } через 2с,
// а второй промис возвращает объект {age: 16} через 3 с.
// Получите результаты работы двух промисов, объедините свойства объектов
// и выведите в консоль
{
  console.group("task 10");

  Promise.all([
    new Promise(resolve => setTimeout(() => resolve({ name: "Anna" }), 2000)),
    new Promise(resolve => setTimeout(() => resolve({ age: 16 }), 3000))
  ]).then(result => console.log(result));

  console.groupEnd();
}
// Task 11
// Используйте предыдущее задание. Пусть теперь второй промис переходит в
// состояние rejected со значением "Promise Error". Измените код, чтобы обработать
// эту ситуацию.
{
  console.group("task 11");

  Promise.allSettled([
    new Promise(resolve => setTimeout(() => resolve({ name: "Anna" }), 2000)),
    new Promise((resolve, reject) =>
      setTimeout(() => reject("Promise Error"), 3000)
    )
  ]).then(
    result => console.log(result),
    error => console.log(error)
  );

  console.groupEnd();
}
// Task 12
// Создайте промис, который перейдет в состояние resolve через 5с и вернет строку
// 'Promise Data'.
// Создайте второй промис, который перейдет в состояние rejected по клику на
// кнопку. Добавьте обработчик для кнопки.
// Используя метод race организуйте отмену промиса.
{
  console.group("task 12");

  console.groupEnd();
}

console.groupEnd();
