console.group("Topic: DOM");

// Task 01
// Найти элемент с id= "t01". Вывести в консоль.
// Найти родительский элемент и вывести в консоль.
// Найти дочерние ноды, если они есть, и вывести в консоль
// названия и тип нод.
{
  console.group("task 01");

  const elemT01 = document.getElementById("t01");
  console.log(elemT01);

  const parentElem = elemT01.parentElement;
  console.log(parentElem);

  const childNodes = elemT01.childNodes;
  for (var i = 0; i < childNodes.length; i++) {
    console.log(childNodes[i]);
  }

  console.groupEnd();
}
// Task 02
// Подсчитать количество <li> элементов на странице. Для поиска элементов использовать
// getElementsByTagName(). Вывести в консоль.
// Добавить еще один элемент в список и вывести снова их количество.
{
  console.group("task 02");

  const arrLi = document.getElementsByTagName("li");
  console.log(arrLi.length);
  const li = document.createElement("li");
  const ul = document.querySelector("ul");
  ul.append(li);
  console.log(arrLi.length);

  console.groupEnd();
}
// Task 03
// Получить элементы <li> используя метод querySelectorAll() и вывети их в консоль
// Добавить новый <li> и снова вывести в консоль
{
  console.group("task 03");

  const allLiBefore = document.querySelectorAll("li");
  console.log(allLiBefore);
  const li = document.createElement("li");
  const ul = document.querySelector("ul");
  ul.append(li);
  const allLiAfter = document.querySelectorAll("li");
  console.log(allLiAfter);

  console.groupEnd();
}
// Task 04
// Найти все первые параграфы в каждом диве и установить цвет фона #ffff00
{
  console.group("task 04");

  const firstP = document.querySelectorAll("div p:first-child");

  firstP.forEach((elem) => {
    elem.style.backgroundColor = "#ffff00";
  });

  console.groupEnd();
}
// Task 05
// Подсчитать сумму строки в таблице и вывести ее в последнюю ячейку
{
  console.group("task 05");

  const td = document.querySelectorAll("td");
  const arrayTd = Array.from(td);
  const numberArr = arrayTd.map((elem) => {
    return Number(elem.innerText);
  });

  function sum(elem, nextElem) {
    return elem + nextElem;
  }

  const summa = numberArr.reduce(sum, 0).toFixed(1);
  const lastElemTd = document.querySelector("tr td:last-child");
  lastElemTd.textContent = summa;

  console.log(lastElemTd);

  console.groupEnd();
}
// Task 06
// Вывести значения всех атрибутов элемента с идентификатором t06
{
  console.group("task 06");

  const elem = document.querySelector("#t06");
  const attr = elem.attributes;
  const arrayAttr = Array.from(attr);
  const valueAttr = arrayAttr.map((attr) => {
    console.log(attr);
    return attr.value;
  });

  console.groupEnd();
}
// Task 07
// Получить объект, который описывает стили, которые применены к элементу на странице
// Вывести объект в консоль. Использовать window.getComputedStyle().
{
  console.group("task 07");

  const elem = document.querySelector("section");
  const style = window.getComputedStyle(elem);

  console.log(style);

  console.groupEnd();
}
// Task 08
// Установите в качестве контента элемента с идентификатором t08 следующий параграф
// <p>This is a paragraph</>
{
  console.group("task 08");

  const elem = document.querySelector("#t08");
  const pElem = document.createElement("p");
  pElem.textContent = "This is a paragraph";
  elem.append(pElem);

  console.groupEnd();
}
// Task 09
// Создайте элемент <div class='c09' data-class='c09'> с некоторым текстовым контентом, который получить от пользователя,
// с помощью prompt, перед элементом с идентификатором t08,
// когда пользователь кликает на нем
{
  console.group("task 09");

  const elem = document.createElement("div");
  elem.setAttribute("class", "c09");
  elem.setAttribute("data-class", "c09");

  const elemByIdT08 = document.querySelector("#t08");
  elemByIdT08.addEventListener("click", addDiv);

  function addDiv() {
    const content = prompt("Введите что-нибудь!");
    elem.textContent = content;
    elemByIdT08.before(elem);
  }

  console.groupEnd();
}
// Task 10
// Удалите у элемента с идентификатором t06 атрибут role
// Удалите кнопку с идентификатором btn, когда пользователь кликает по ней
{
  console.group("task 10");

  const elem = document.querySelector("#t06");
  elem.removeAttribute("role");

  const btnClick = document.querySelector("button");

  btnClick.addEventListener("click", deleteBtn);

  function deleteBtn() {
    document.querySelector("#btn").remove();
  }

  console.groupEnd();
}
console.groupEnd();
