console.group("Topic: Strings");

// Task 01. padStart
// RU: Объявите три переменных: hour, minute, second.
//     Присвойте им следующие значения: 4, 35, 5.
//     Выведите в консоль время в формате 04:35:05.
// EN: Declare three variables: hour, minute, second.
//     Assign them the following values: 4, 35, 5.
//     Display the time in the format 04:35:05 in the console.
{
  console.group("task 01");

  const hour = 4;
  const minute = 35;
  const second = 5;

  console.log(`${0}${hour}:${minute}:${0}${second}`);

  console.groupEnd();
}

// Task 02. repeat
// RU: Создайте функцию, которая выведет в консоль пирамиду на 9 уровней как показано ниже
//     1
//     22
//     333
//     4444
//     ...
// EN: Create a function which displays a 9 level pyramid in the console according to the
//     following pattern
//     1
//     22
//     333
//     4444
//     ...
{
  console.group("task 02");

  function repeat(a) {
    for (let chet = 1; chet <= 9; chet++) {
      console.log(chet.toFixed().repeat(chet));
    }
  }
  repeat();

  console.groupEnd();
}

// Task 03. includes
// RU: Напишите код, который выводит в консоль true, если строка str содержит
//     'viagra' или 'XXX', а иначе false.
//     Тестовые данные: 'buy ViAgRA now', 'free xxxxx'
// EN: Create a snippet of code which displays the value true in the console
//     when str contains 'viagra' or 'XXX', otherwise it displays false.
{
  console.group("task 03");

  const str1 = "buy ViAgRA now".toLowerCase();
  const str2 = "free xxxxx".toUpperCase();

  function search(a) {
    const result = (a.includes("viagra") === true || a.includes("XXX") === true)
      ? true
      : false;
    return result;
  }

  console.log(search(str1));
  console.log(search(str2));

  console.groupEnd();
}

// Task 04. includes + index
// RU: Проверить, содержит ли строка второе вхождение подстроки,
//     вернуть true/false.
// EN: Check whether the string contains a second occurrence of a substring,
//     return true / false.
{
  console.group("task 04");

  const str = "комары пьют кровь комаров";
  const searchStr = "комар";

  function lineCheck(line, substring) {
    line.indexOf(substring);
    substring.length;

    return line.includes(substring, line.indexOf(substring) + substring.length);
  }

  console.log(lineCheck(str, searchStr));

  console.groupEnd();
}

// Task 05. Template literal
// RU: Создать строку: "ten times two totally is 20"
//     используя переменные:
//     const a = 10;
//     const b = 2;
//     и template literal
// EN: Create s string "ten times two totally is 20"
//     using the following variables:
//     const a = 10;
//     const b = 2;
//     and template literal
{
  console.group("task 05");

  const a = 10;
  const b = 2;

  console.log(`${a} times ${b} totally is ${a * b}`);

  console.groupEnd();
}

// Task 06. normalize
// RU: Создайте функцию, которая сравнивает юникод строки.
//     Сравните две строки
//     var str1 = '\u006d\u0061\u00f1';
//     var str2 = '\u006d\u0061\u006e\u0303';
// EN: Create a function that compares the unicode strings.
//     Compare 2 strings:
//     var str1 = '\u006d\u0061\u00f1';
//     var str2 = '\u006d\u0061\u006e\u0303';
{
  console.group("task 06");

  var str1 = "\u006d\u0061\u00f1";
  var str2 = "\u006d\u0061\u006e\u0303";

  function comparison(a, b) {
    if (a > b) {
      return `${a} больше ${b}`;
    } else if (a < b) {
      return `${a} меньше ${b}`;
    } else {
      return `${a} равно ${b}`;
    }
  }

  console.log(comparison(str1, str2));

  console.groupEnd();
}

// Task 07. endsWith
// RU: Создайте функцию, которая на вход получает массив имен файлов и расширение файла
//     и возвращает новый массив, который содержит файлы указанного расширения.
// EN: Create a function that gets an array of file names and a file extension as its parameters
//     and returns a new array that contains the files of the specified extension.
{
  console.group("task 07");

  const arr = [
    ["picture", "jpg"],
    ["DSC1029", "png"]
  ];

  function unionOfArrays(a) {
    let newArr = [];

    for (let i = 0; i < a.length; i++) {
      newArr.push(a[i].join("."));
    }

    console.log(newArr);
  }

  unionOfArrays(arr);

  console.groupEnd();
}

// Task 08. String.fromCodePoint
// RU: Создать функцию, которая выводит в консоль строчку в формате 'символ - код'
//     для кодов в диапазоне 78000 - 80000.
// EN: Create a function that displays a line in the format 'character - code' to the console
//     for codes in the range of 78000 - 80000.
{
  console.group("task 08");

  function conversion() {
    for (let i = 78000; i <= 80000; i++) {
      console.log(`${String.fromCodePoint(i)} - ${i}`);
    }
  }

  conversion();

  console.groupEnd();
}

// Task 09
// RU: Создайте функцию, которая должна выводить в консоль следующую пирамиду
//     Пример:
//     pyramid(1) = '#'
//
//     pyramid(2) = ' # '
//                  '###'
//
//     pyramid(3) = '  #  '
//                  ' ### '
//                  '#####'
// EN: Create a function that should display the next pyramid in the console
//     Example:
//     pyramid(1) = '#'
//
//     pyramid(2) = ' # '
//                  '###'
//
//     pyramid(3) = '  #  '
//                  ' ### '
//                  '#####'
{
  console.group("task 09");

  function pyramid(a) {
    let j = a;
    for (let i = 1; i <= a; i++) {
      j--;
      if (i === 1) {
        console.log(`${" ".repeat(j)}${"#".repeat(i)}${" ".repeat(j)}`);
      } else {
        console.log(
          `${" ".repeat(j)}${"#".repeat(i + (i - 1))}${" ".repeat(j)}`
        );
      }
    }
  }

  pyramid(1);
  pyramid(2);
  pyramid(3);

  console.groupEnd();
}
// Task 10
// RU: Создайте тег-функцию currency, которая формитирует числа до двух знаков после запятой
//     и добавляет знак доллара перед числом в шаблонном литерале.
// EN: Create a currency tag function that forms numbers up to two decimal digits.
//     and adds a dollar sign before the number in the template literal.
{
  console.group("task 10");

  function currency(dollar, numb) {
    const currencyValue = dollar[0];
    return `${currencyValue}${numb.toFixed(2)}`;
  }

  const outputNumb = currency`$${2.12345}`;
  console.log(outputNumb);

  console.groupEnd();
}

console.groupEnd();
