console.group("Topic: Arrays");

// Task 01
// RU: Создать массив. Получить последний элемент массива.
//     1.    Без удаления этого элемента из массива.
//     2.    С удалением этого элемента из массива
//    Выведите массивы в консоль
// EN: Create an array of any elements. Get the last element from this array.
//     1.    without deleting this element from an array;
//     2.    with deleting this element from an array.
//     Display them in the console.
{
  console.group("task 01");

  const arr = ["Audi", "BMW", "Honda", "Toyota"];
  const elem1 = arr[arr.length - 1];
  console.log(arr);
  const elem2 = arr.pop();
  console.log(arr);

  console.groupEnd();
}

// Task 02
// RU: Создать массив любых элементов. Добавить элемент в конец массива.
//     1. Модифицировать текущий массив
//     2. Создать новый массив
//     Выведите массивы в консоль
// EN: Create an array of any elements. Add new element to the end of this array.
//     1. mutate current array;
//     2. create a new array.
//     Disply them in the conole.
{
  console.group("task 02");

  const arr = ["Audi", "BMW", "Honda", "Toyota"];
  console.log(arr);
  const newElem = arr.push("Mercedes");
  const newArr = arr;
  console.log(newArr);

  console.groupEnd();
}

// Task 03
// RU: Создать массив любых элементов. Вставить новый элемент под индексом 3.
//     1. Модифицировать текущий массив
//     2. Создать новый массив
//     Выведите массивы в консоль
// EN: Create an array of any elements. Insert a new element with index 3.
//     1. mutate current array;
//     2. create a new array.
//     Disply them in the conole.
{
  console.group("task 03");

  const arr = ["Audi", "BMW", "Honda", "Toyota"];
  console.log(arr);
  arr[3] = "Ferrari";
  const newArr = arr;
  console.log(newArr);

  console.groupEnd();
}

// Task 04
// RU: Создать массив любых элементов.
//     Обойти элементы массива и вывести их в консоль.
// EN: Create an array of any elements.
//     Iterate over this array and display each element in the console.
{
  console.group("task 04");

  const arr = ["Audi", "BMW", "Honda", "Toyota"];
  for (let i = 0; i < arr.length; i++) {
    console.log(arr[i]);
  }

  console.groupEnd();
}

// Task 05
// RU: Создать массив чисел в диапазоне от 0 до 100.
//     Подсчитать и вывети сумму тех элементов, которые больше 50.
// EN: Create an array of numbers in the range from 0 to 100.
//     Calculate and display the sum of the elements, which are greater than 50.
{
  console.group("task 05");

  const arr = [2, 51, 37, 52, 53];
  const arrFilt = arr.filter(item => item > 50);
  console.log(arrFilt);
  const sum = arrFilt.reduce((sum, current) => sum + current);
  console.log(sum);

  console.groupEnd();
}

// Task 06
// RU: Создать массив строк. На основе этого массива создать строку –
//     объдинить все элементы массива, используя определенный разделитель.
// EN: Create an array of strings. Create a string on the basis of this array.
//     This string should contain all elements from an array separated by certain delimeter.
{
  console.group("task 06");

  const arr = ["Audi", "BMW", "Honda", "Toyota"];
  const str = arr.join(",");
  console.log(str);

  console.groupEnd();
}

// Task 07
// RU: Создать массив чисел от 1 до 20 в случайном порядке.
//     Отcортировать массив по возрастанию. Вывести его в консоль.
//     Получить массив, отсортрованный в обратном порядке, и вывести его в консоль.
// EN: Create an array of numbers in the range from 1 to 20 in random order.
//     Sort this array in ascending order. Display it in the console.
//     Create a copy of the previous array in reverse order. Disply it in the console.
{
  console.group("task 07");

  const arr = [2, 19, 5, 15, 10, 3, 9];
  const newArr1 = arr.sort((a, b) => {
    if (a < b) {
      return -1;
    } else if (a > b) {
      return 1;
    } else {
      return 0;
    }
  });

  console.log(newArr1);
  const newArr2 = newArr1.reverse();
  console.log(newArr2);

  console.groupEnd();
}

// Task 08
// RU: Создать массив [3, 0, -1, 12, -2, -4, 0, 7, 2]
//     На его основе создать новый массив [-1, -2, -4, 0, 0, 3, 12, 7, 2].
//     первая часть - отрицательные числа в том же порядке
//     вторая часть - нули
//     третья часть - положительные числа в том же порядке.
// EN: Create the array: [3, 0, -1, 12, -2, -4, 0, 7, 2]
//     Use this array and create new one: [-1, -2, -4, 0, 0, 3, 12, 7, 2].
//     First part - negative values from the original array in the same order,
//     Next part - zeroes
//     Last part - positive values from the original array in the same order.
{
  console.group("task 08");

  const arr = [3, 0, -1, 12, -2, -4, 0, 7, 2];

  function createNewArr(array) {
    const arrMin = [];
    const arrNull = [];
    const arrMax = [];

    array.forEach(function (elem) {
      if (elem < 0) {
        arrMin.push(elem);
      } else if (elem > 0) {
        arrMax.push(elem);
      } else {
        arrNull.push(elem);
      }
    });

    const mainArr = [...arrMin, ...arrNull, ...arrMax];

    return mainArr;
  }

  console.log(createNewArr(arr));

  console.groupEnd();
}

// Task 09
// RU: 1. Создайте массив styles с элементами "Jazz", "Blues".
//     2. Добавьте в конец значение "Rock-n-Roll".
//     3. Замените предпоследнее значение с конца на "Classics".
//     4. Удалите первый элемент из массива и выведите его в консоль.
//     5. Добавьте в начало два элемента со значениями "Rap" и "Reggae".
//     6. Выведите массив в консоль.
// EN: 1. Create an array styles with two elements "Jazz", "Blues".
//     2. Add new element "Rock-n-Roll" to the end of the array.
//     3. Replace the last but one element with new value "Classics".
//     4. Remove the first element from the array and disply it in the console.
//     5. Add two new elements "Rap" and "Reggae" at the begining of the array.
//     6. Display an array in the console.
{
  console.group("task 09");

  const styles = ["Jazz", "Blues"];
  styles.push("Rock-n-Roll");
  styles[styles.length - 2] = "Classics";
  console.log(styles.shift());
  styles.unshift("Rap", "Reggae");
  console.log(styles);

  console.groupEnd();
}

// Task 10
// RU: Подсчитать в строке "dskjdhfkjshdfkjhsdkjureyteiruyiqywehjkh"
//     отдельно количество букв r, k, t и вывести в консоль.
// EN: Calculate the number of letters r, k, t in this string
//     "dskjdhfkjshdfkjhsdkjureyteiruyiqywehjkh" and display them in the console.
{
  console.group("task 10");

  const str = "dskjdhfkjshdfkjhsdkjureyteiruyiqywehjkh";
  const arr = str.split("");
  const newArrR = arr.filter(item => item === "r");
  const newArrK = arr.filter(item => item === "k");
  const newArrT = arr.filter(item => item === "t");
  console.log(newArrR.length);
  console.log(newArrK.length);
  console.log(newArrT.length);

  console.groupEnd();
}

// Task 11
// RU: Создать массив любых элементов.
//     Получить случайный элемент из массива и вывести его в консоль.
// EN: Create an array of any elements.
//     Get the random element from this array and display it in the console.
{
  console.group("task 11");

  const arr = ["Audi", "BMW", "Honda", "Toyota"];
  console.log(arr[2]);

  console.groupEnd();
}

// Task 12
// RU: Создать двумерный массив:
//     1 2 3
//     4 5 6
//     7 8 9
//     Вывести его в консоль.
// EN: Create two-dementional array:
//     1 2 3
//     4 5 6
//     7 8 9
//     Display it in the console.
{
  console.group("task 12");

  const arrBig = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
  ];
  console.log(arrBig);

  console.groupEnd();
}

// Task 13
// RU: Преобразовать массив из предыдущего задания в одномерный.
//     Вывести его в консоль
// EN: Transform an array from the previous task into one-dementional array.
//     Display it in the console.
{
  console.group("task 13");

  const arrBig = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
  ];
  const newArrBigg = arrBig.flat();
  console.log(newArrBigg);

  console.groupEnd();
}

// Task 14
// RU: Создать массив любых элементов.
//     На его основе получить новый массив – подмножество элементов
//     оригинального массива начиная с индекса a и заканчивая индексом b.
//     Вывести массив в консоль.
// EN: Create an array of any elements.
//     Create new one on the basis of the originl array. New array should
//     contain elements from index a to index b.
//     Display it in the console.
{
  console.group("task 14");

  const arr = [3, 0, -1, 12, -2, -4, 0, 7, 2];

  function conversion(array, indexStart, indexFinish) {
    const newArr = array.slice(indexStart, indexFinish);
    return newArr;
  }
  console.log(conversion(arr, 1, 5));

  console.groupEnd();
}

// Task 15
// RU: Создать массив любых элементов.
//     Найти индекс указаного элемента в массиве и вывести его в консоль.
// EN: Create an array of any elements.
//     Find the index of a particular element in the array and disply it in the console.
{
  console.group("task 15");

  const arr = [3, 0, -1, 12, -2, -4, 0, 7, 2];

  function showIndex(array, elem) {
    return array.indexOf(elem);
  }
  console.log(showIndex(arr, 12));

  console.groupEnd();
}

// Task 16
// RU: Создать массив с дублями элементов. На его основе создать новый массив
//     уникальных элементов (удалить дубли).
//     Вывести новый массив в консоль.
// EN: Create an array with duplicate elements. Create new one on the basis of the originl array.
//     Remove duplicated elements.
//     Display it in the console.
{
  console.group("task 16");

  const arr = [3, 3, 5, 12, 3, 5, 7, 7, 12];

  function duplicateRemoval(array) {
    const newArr = [];

    for (let value of array) {
      if (!newArr.includes(value)) {
        newArr.push(value);
      }
    }
    return newArr;
  }

  console.log(duplicateRemoval(arr));

  console.groupEnd();
}

// Task 17
// RU: Создать массив с дублями. Найти первый элемент, который дублируется.
//     Заменить этот элемент и все его копии на символ '*'.
//     Вывести массив в консоль.
// EN: Create an array with duplicate elements. Find first duplicated element.
//     Replace this element and all its copies with symbol '*'.
//     Display it in the console.
{
  console.group("task 17");

  console.groupEnd();
}
// Task 18
// RU: Создать массив целых чисел. На его основе создать массивы – представления
//     этих же чисел в бинарном, восьмеричном и шестнадцатеричном виде.
//     Вывести эти массивы в консоль.
// EN: Create an array of integer numbers. Create 3 new ones on the basis of the originl array.
//     First array contains the binary representation of the elements from the original array.
//     Second array contains the octal representation of the elements from the original array.
//     Third array contains the hexadecimal representation of the elements from the original array.
//     Display them in the console.

{
  console.group("task 18");

  const arr = [1, 7, 5, 3, 17, 70, 22, 12];

  const newArrBin = arr.map(item => parseInt(item, 2));
  const newArrOct = arr.map(item => parseInt(item, 8));
  const newArrHex = arr.map(item => parseInt(item, 16));

  console.log(newArrBin);
  console.log(newArrOct);
  console.log(newArrHex);

  console.groupEnd();
}

// Task 19
// RU: Получить из строки 'a big brown fox jumps over the lazy dog' массив слов,
//     который содержит элементы, длина которых не больше 3 символа.
//     Вывести массив в консоль.
// EN: Get the array of words from the string 'a big brown fox jumps over the lazy dog'.
//     This array should contain only words, the length of which is 3 or less characters.
//     Display it in the console.
{
  console.group("task 19");

  const str = "a big brown fox jumps over the lazy dog";

  function conversion(string) {

    const arr = string.split(" ").filter(item => item.length <= 3);

    return arr;
  }

  console.log(conversion(str));

  console.groupEnd();
}

// Task 20
// RU: Создать массив, который содержит строки и числа.
//     Проверить, содержит ли массив только строки.
//     Вывети результат в консоль
// EN: Create an array of numbers and strings.
//     Check whether this array contains only strings.
//     Display the result in the console.
{
  console.group("task 20");

  const arr = ["one", 3, 5, "two", 5, "hello", 7];

  function check(array) {

    const arr = array.every(item => typeof item === "string");

    return arr;
  }

  console.log(check(arr));

  console.groupEnd();
}

// Task 21
// RU: Создать отсортированный массив чисел.
//     Реализовать функцию binarySearch(arr, value), которая принимает массив
//     и значение и возвращает индекс значения в массиве или -1.
//     Функция должна использовать бинарный поиск.
//     Вывести результат в консоль.
// EN: Create an array of numbers in sort order.
//     Implement function binarySearch(arr, value), which takes an array
//     and a value and returns the index of this value in the array or -1.
//     Function should use binary search.
//     Display the result in the console.
{
  console.group("task 21");

  const array = [1, 2, 3, 4, 5, 6, 7];

  function binarySearch(arr, value) {
    let firstElem = 0;
    let lastElem = arr.length - 1;
    let position = -1;
    let found = false;
    let middle;

    while (found === false && firstElem <= lastElem) {
      middle = Math.floor((firstElem + lastElem) / 2);

      if (arr[middle] === value) {
        found = true;
        position = middle;
      } else if (arr[middle] > value) {
        lastElem = middle - 1;
      } else {
        firstElem = middle + 1;
      }
    }
    console.log(position);
    return position;
  }

  binarySearch(array, 4);

  console.groupEnd();
}

console.groupEnd();
